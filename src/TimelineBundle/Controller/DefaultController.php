<?php

namespace TimelineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use TimelineBundle\Entity\Post;


class DefaultController extends AbstractController
{
    /**
     * Common function for all controller to authenticate user
     */
    public function autheticateUser($user)
    {
        // Checking if data is of correct format
        try{
            $user_id = $user->id;
            $token = $user->token;
            $token_data = $this->getDoctrine()
                               ->getRepository('UserBundle:Token')
                               ->findOneBy(["value"=>$token]);

            if($token_data->getUserId()->get_id() === (int)$user_id && $token_data->getTimestamp()+(2*24*60*60) > time()){
                return $token_data->getUserId();
            }else{
                return -2;
            }
        }catch (\Exception $e){
            return -1; //Badly formatted data
        }
    }

    /**
     * @Route("/get", name="get_timeline")
     */
    public function indexAction(Request $request)
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
            return new JsonResponse(["error" => "wrong method"], 405);
        }

        // Getting request data
        try{
            $headers = $request->headers->all();
            $user = json_decode($headers['authorization'][0]);
        }catch (\Exception $e){
            return new JsonResponse(["error" => "badly formatted data"], 400);
        }

        // Authenticate user
        $user = $this->autheticateUser($user);

        if($user === -1){
            return new JsonResponse(["error" => "badly formatted data"], 401);
        }else if($user === -2){
            return new JsonResponse(["error" => "unauthorized user"], 401);
        }

        // Retrieve data from database
        $posts = $this->getDoctrine()
                      ->getRepository("TimelineBundle:Post")
                      ->findBy(["userId"=>$user->get_id()],["timestamp" => "desc"]);

        $response = [];
        foreach($posts as $post){
            $id = (int)$post->getId();
            $description = htmlentities(trim($post->getDescription()));
            $path = $post->getPath();
            $time = date('l jS \of F Y h:i:s A',$post->getTimestamp());
            array_push($response, ["id"=>$id, "description"=>$description, "path"=>$path, "time"=>$time]);
        }

        return new JsonResponse($response);
    }


    /**
     * @Route("/upload", name="upload_image")
     */
    public function uploadAction(Request $request)
    {

        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            return new JsonResponse(["error" => "wrong method"], 405);
        }

        // Getting data
        try{
            $headers = $request->headers->all();
            $user = json_decode($headers['authorization'][0]);
            $file = $request->files->get('file');
        }catch (\Exception $e){
            return new JsonResponse(["error" => "badly formatted data"], 400);
        }


        // Authenticate user
        $user = $this->autheticateUser($user);

        if($user === -1){
            return new JsonResponse(["error" => "badly formatted data"], 401);
        }else if($user === -2){
            return new JsonResponse(["error" => "unauthorized user"], 401);
        }

        // Verify id file is correct
        $allowed_types = ['image/png',
                          'image/jpeg',
                          'image/jpeg',
                          'image/jpeg',
                          'image/gif',
                          'image/bmp',
                          'image/vnd.microsoft.icon',
                          'image/tiff',
                          'image/tiff',
                          'image/svg+xml',
                          'image/svg+xml'];

        if(!$file->isValid() || !in_array($file->getMimeType(),$allowed_types)){
            return new JsonResponse(["error" => "file not proper"], 400);
        }

        //File name

        $file_name = $user->get_id()."".(int)(microtime(true)*1000).".".$file->guessExtension();


        // Adding file details to the table

        $em = $this->getDoctrine()->getManager();
        $post = new Post();
        $post->setUserId($user)->setTimestamp(time())->setPath($file_name);

        $em->persist($post);
        $em->flush();

        // Moving file to path
        $file->move(__DIR__."/../../../uploads", $file_name);

        return new JsonResponse(["id"=>$post->getId()]);
    }

    /**
     * @Route("/image/{id}", name="show_image")
     */
    public function showImageAction(Request $request, $id){

        if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
            return new JsonResponse(["error" => "wrong method"], 405);
        }

        $id = (int)$id;
        // Getting request data
        try{
            $headers = $request->headers->all();
            $user = json_decode($headers['authorization'][0]);
        }catch (\Exception $e){
            return new JsonResponse(["error" => "badly formatted data"], 400);
        }

        // Authenticate user
        $user = $this->autheticateUser($user);

        if($user === -1){
            return new JsonResponse(["error" => "badly formatted data"], 401);
        }else if($user === -2){
            return new JsonResponse(["error" => "unauthorized user"], 401);
        }

        // Sending the image
        $post = $this->getDoctrine()->getRepository("TimelineBundle:Post")->find($id);
        $path = $post->getPath();
        $file = file_get_contents(__DIR__."/../../../uploads/".$path);
        $base64 = 'data:image/' . explode(".",$path)[1] . ';base64,' . base64_encode($file);
        $headers = array(
            'Content-Type'     => 'image/png',
            'Content-Disposition' => 'inline; filename="'.$path.'"');
        return new Response($base64, 200, $headers);

    }

    /**
     * @Route("/post", name="post_to_timeline")
     */
    public function postAction(Request $request){
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            return new JsonResponse(["error" => "wrong method"], 405);
        }

        // Getting request data
        try{
            $data = json_decode($request->getContent(), true);
            $headers = $request->headers->all();
            $user = json_decode($headers['authorization'][0]);
        }catch (\Exception $e){
            return new JsonResponse(["error" => "badly formatted data"], 400);
        }

        // Authenticate user
        $user = $this->autheticateUser($user);

        if($user === -1){
            return new JsonResponse(["error" => "badly formatted data"], 401);
        }else if($user === -2){
            return new JsonResponse(["error" => "unauthorized user"], 401);
        }

        // Sanitize data
        $id = (int)$data['postId'];
        $description = htmlentities(trim($data['description']));

        // Try updating database
        $post = new Post();
        try{
            $em = $this->getDoctrine()->getManager();
            if($id===0){
                $post->setUserId($user)->setDescription($description)->setTimestamp(time());
                $em->persist($post);
            }else{
                $post = $em->getRepository("TimelineBundle:Post")->find($id);
                if($post->getDescription() === null){
                    $post->setDescription($description);
                }else{
                    return new JsonResponse(["error" => "not allowed to edit already created post"], 403);
                }
            }
            $em->flush();
        }catch (\Exception $e){
            // Invalid id
            return new JsonResponse(["error" => "badly formatted data"], 400);
        }

        // Return post details
        $id = (int)$post->getId();
        $description = htmlentities(trim($post->getDescription()));
        $path = $post->getPath();
        $time = date('l jS \of F Y h:i:s A',$post->getTimestamp());
        return new JsonResponse(["id"=>$id, "description"=>$description, "path"=>$path, "time"=>$time]);
    }
}
