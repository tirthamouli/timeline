<?php
namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function get_id()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(name="first_name", type="string", length=255)
     * @Assert\NotBlank()
     *
     */
    protected $first_name;

    public function get_first_name()
    {
        return $this->first_name;
    }

    public function set_first_name($value)
    {
        $this->first_name = $value;
    }

    /**
     * @ORM\Column(name="last_name", type="string", length=255)
     * @Assert\NotNull()
     */
    protected $last_name;

    public function get_last_name()
    {
        return $this->last_name;
    }

    public function set_last_name($value)
    {
        $this->last_name = $value;
    }

    /**
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true )
     */
    protected $email;

    public function get_email()
    {
        return $this->email;
    }

    public function set_email($value)
    {
        $this->email = $value;
    }

    /**
     * @ORM\Column(name="password", type="string", length=512)
     * @Assert\NotNull()
     */
    protected $password;

    public function get_password()
    {
        return $this->password;
    }

    public function set_password($value)
    {
        $this->password = $value;
    }

}
