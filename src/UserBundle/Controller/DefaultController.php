<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validation;
use UserBundle\Entity\Token;
use UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;



class DefaultController extends Controller
{
    /**
     * @Route("/signup", name="user_signup")
     *
     */
    public function signupAction(Request $request)
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            return new JsonResponse(["error" => "wrong method"]);
        }

        // Step1: Validate
        $data = json_decode($request->getContent(), true);

        if (!isset($data['first_name']) || !isset($data['last_name']) || !isset($data['email']) || !isset($data['password'])) {
            // Invalid data
            return new JsonResponse(["error" => "incomplete data"]);
        }

        $validator = Validation::createValidator();

        $constraint = new Assert\Collection(array(
            'first_name' => new Assert\Length(array('min' => 1)),
            'last_name' => new Assert\Length(array('min' => 1)),
            'email' => new Assert\Email(),
            'password' => new Assert\Length(array('min' => 8)),
        ));

        $violations = $validator->validate($data, $constraint);

        if (0 !== count($violations)) {
            // Invalid data
            return new JsonResponse(["error" => "invalid data"]);
        }


        // Step2: Insert into database
        $em = $this->getDoctrine()->getManager();
        $user = new User();

        $user->set_first_name($data['first_name']);
        $user->set_last_name($data['last_name']);
        $user->set_email($data['email']);
        $user->set_password(password_hash($data['password'], PASSWORD_DEFAULT));

        try {
            $em->persist($user);
            $em->flush();
        } catch (\Exception $e) {
            return new JsonResponse(["error" => "email already registered"]);
        }

        return new JsonResponse(["success" => "successfully registered"]);


        // DELETE THIS (update)
        // $em = $this->getDoctrine()->getManager();
        // $user = $em->getRepository("UserBundle:User")->find(2);
        // $user->set_first_name("Tirth");
        // $em->remove($user); //for deleting user
        // $em->flush();
    }

    /**
     * @Route("/login", name="user_login")
     *
     */
    public function loginAction(Request $request)
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            return new JsonResponse(["error" => "wrong method"]);
        }

        $data = json_decode($request->getContent(), true);

        if (!isset($data['email']) || !isset($data['password'])) {
            // Invalid data
            return new JsonResponse(["error" => "incomplete data"]);
        }

        $email = $data['email'];
        $password = $data['password'];

        //Step 1: Checking if user has provided valid details
        $user = $this->getDoctrine()->getRepository("UserBundle:User")->findOneBy([
            "email" => $email
        ]);

        if (is_null($user) || !password_verify($password, $user->get_password())) {
            return new JsonResponse(["error" => "wrong username or password"]);
        }

        // Step2: generate a new token for user
        $token_value = bin2hex(random_bytes(64));

        $em = $this->getDoctrine()->getManager();
        $token = new Token();

        $token->setUserId($user)
            ->setValue($token_value)
            ->setTimestamp(time());

        $em->persist($token);
        $em->flush();

        return new JsonResponse(["id" => $user->get_id(), "token" => $token_value]);

    }


}
